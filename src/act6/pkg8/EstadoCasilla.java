package act6.pkg8;

public enum EstadoCasilla {
    FICHA_O{
        @Override
        public String toString() {
            return Jugador.BLUE+"O"+Jugador.RESET;
        }
    
    } , FICHA_X{
        @Override
        public String toString() {
            return Jugador.RED+"X"+Jugador.RESET;
        }
    } , VACIO{
        @Override
        public String toString() {
            return " ";
        }
    };
}


