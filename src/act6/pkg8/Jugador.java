package act6.pkg8;



public class Jugador {
    private EstadoCasilla ficha;
    public static final String RED = "\u001B[31m";
    public static final String RESET = "\u001B[0m";
    public static final String BLUE = "\u001B[34m";
    public static final String VERDE = "\033[32m";
    
    
    public Jugador(EstadoCasilla ficha) {
        this.ficha = ficha;
    }
    
    public void ponerFicha(Tablero tablero){
        Coordenada coordenada = recogerCoordenada(tablero);
        boolean correcto = false;
        while(correcto == false){
        if(tablero.isOcupada(coordenada) == false){
            tablero.getTablero()[coordenada.getFila()-1][coordenada.getColumna()-1] =  this.ficha;
                    
//            tablero.tablero[coordenada.getFila()-1][coordenada.getColumna()-1] = this.ficha;
            correcto = true;
        } else{
            System.out.println(RED +"�Error!"+ RESET +" Coordenada ocupada en el tablero");
            coordenada = recogerCoordenada(tablero);
        }
        }
        
    }
    
    private Coordenada recogerCoordenada(Tablero tablero){
        Coordenada coordenada = new Coordenada(recogerInt("fila",tablero), recogerInt("columna",tablero));
    return coordenada;
    }
    
    private int recogerInt(String filCol,Tablero tablero){
        int opcion = 0;
        boolean esCorrecte = false;
        System.out.printf("\nJugador con %s\n",this.ficha.toString());
        while(esCorrecte == false){
            System.out.printf("Introduce %s [%d-%d]: ",filCol,1,Tablero.DIMENSION);
            
        if(TresEnRaya.teclado.hasNextInt()){
            opcion = TresEnRaya.teclado.nextInt();
            if(opcion > 0 && opcion <= Tablero.DIMENSION){
                esCorrecte = true;
            }
            else{
                System.out.println(RED +"�Error!"+ RESET +" Introduce una coordenada v�lida");
            }
        } else{
            System.out.println("�Error! Debe introducir un n�mero entero");
            TresEnRaya.teclado.next();
        }
        
        }
    return opcion;
    }

    public void cantarVictoria(){
            System.out.printf("\n�El jugador %s es el ganador!\n\n",this.ficha);
    }
}
