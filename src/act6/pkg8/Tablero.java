package act6.pkg8;

public class Tablero {

    public final static int DIMENSION = 3 ;
    private EstadoCasilla[][] tablero = new EstadoCasilla[DIMENSION][DIMENSION];

    public Tablero() {
        vaciar();
    }

    public EstadoCasilla[][] getTablero() {
        return tablero;
    }

    public void mostrar() {
        System.out.print("| |");
        for (int k = 0; k < tablero[0].length; k++) {
            System.out.printf("%d|", k + 1);
        }
        for (int i = 0; i < tablero.length; i++) {
            System.out.printf("\n|%d|", i + 1);
            for (int j = 0; j < tablero[i].length; j++) {
                System.out.printf("%s|", tablero[i][j]);
            }
        }
        System.out.print("\n");
    }

    public void vaciar() {
        for (EstadoCasilla[] tablero1 : tablero) {
            for (int j = 0; j < tablero1.length; j++) {
                tablero1[j] = EstadoCasilla.VACIO;
            }
        }
    }

    public boolean hayTresEnRaya() {
        EstadoCasilla[] jugadores = {EstadoCasilla.FICHA_O, EstadoCasilla.FICHA_X};

        for (EstadoCasilla valor : jugadores) {
            if (hayTresEnRaya(valor)) {
                return true;
            }
        }
        return false;
    }

    private boolean hayTresEnRaya(EstadoCasilla color) {
       //Comprovar files i columnes
        for (int i = 0; i < DIMENSION; i++) {
            boolean filaCompleta = true;
            boolean columnaCompleta = true;
            for (int j = 0; j < DIMENSION; j++) {
                if (tablero[i][j] != color) {
                    filaCompleta = false;
                }
                if (tablero[j][i] != color) {
                    columnaCompleta = false;
                }
            }

            if (filaCompleta || columnaCompleta) {
                return true;
            }
        }
        //Comprovar diagonals
        boolean diagonal1Completa = true;
        boolean diagonal2Completa = true;
       
        for (int i = 0; i < DIMENSION; i++) {
            if (tablero[i][i] != color) {
                diagonal1Completa = false;
            }
            if (tablero[i][DIMENSION - 1 - i] != color) {
                diagonal2Completa = false;
            }
        }
        return diagonal1Completa || diagonal2Completa;
    }

    public boolean isOcupada(Coordenada coordenada) {
        int fila = coordenada.getFila() - 1;
        int columna = coordenada.getColumna() - 1;
        return tablero[fila][columna] != EstadoCasilla.VACIO;
    }

    public void ponerFicha(Coordenada coordenada, EstadoCasilla color) {
        int fila = coordenada.getFila() - 1;
        int columna = coordenada.getColumna() - 1;
        if (isOcupada(coordenada) == false) {
            tablero[fila][columna] = color;
        }
    }

    public boolean estaLleno() {
        int numCasillas = 0;
        int posicionesLlenas = 0;

        for (EstadoCasilla[] filas : tablero) {
            for (EstadoCasilla celda : filas) {
                if (celda != EstadoCasilla.VACIO) {
                    posicionesLlenas++;
                }
                numCasillas++;
            }
        }
        return numCasillas == posicionesLlenas;
    }

}
