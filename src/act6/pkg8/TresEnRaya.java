package act6.pkg8;

import java.util.Scanner;

 public class TresEnRaya {
    public static Scanner teclado;
    
    private Tablero tablero;
    private Jugador[] jugadores;

    public TresEnRaya() {
        this.tablero = new Tablero();
        this.jugadores = new Jugador[2];
    }
    
    public static void main(String[] args) {
        teclado = new Scanner(System.in);
        TresEnRaya tresEnRaya = new TresEnRaya();
        tresEnRaya.jugar();
        
    }
    
    public void jugar() {
          mostrarBienvenida();
          limpiarPantalla();
        System.out.printf("\n�Bienvenido al juego del %d en raya!\n\n", Tablero.DIMENSION);
        boolean juegoAcabado = false;
        
        
        Jugador j1 = new Jugador(EstadoCasilla.FICHA_O);
        Jugador j2 = new Jugador(EstadoCasilla.FICHA_X);
        jugadores[0] = j1;
        jugadores[1] = j2;
        tablero.mostrar();
        while (!juegoAcabado) {
            for (int i = 0; i < jugadores.length; i++) {
                jugadores[i].ponerFicha(tablero);
                System.out.println();
                
                boolean victoria = tablero.hayTresEnRaya();
                if (victoria) {
                    jugadores[i].cantarVictoria();
                    juegoAcabado = true;
                    tablero.mostrar();
                    break;
                }
                if (tablero.estaLleno() && !victoria) {
                    System.out.println("\n\n�Empate!");
                    tablero.mostrar();
                    juegoAcabado = true;
                    break;
                }
                tablero.mostrar();
            }
        }
        reiniciarJuego();
    }
    
    private void reiniciarJuego() {
        boolean reinicia = false;
        while(!reinicia){
        System.out.println("\n\n�Quieres jugar otra vez? (s/n): ");
        String respuesta = teclado.next();
        if (respuesta.equalsIgnoreCase("s")) {
            System.out.println("\n");
            tablero.vaciar();
            jugar();
            reinicia = true;
        } else if(respuesta.equalsIgnoreCase("n")){
            System.out.println("\n\n�Gracias por jugar!");
            teclado.close();
            reinicia =true;
        } else {
            System.out.println(Jugador.RED+"�Error!"+Jugador.RESET+ " Debes introducir S o N");
        }
      }
    }
    public void mostrarBienvenida(){
        System.out.println(Jugador.VERDE+ " ____ ___ __  __ _____ _   _ ____ ___  __  _   _      _____ _   _         \n" +Jugador.VERDE+
"|  _ \\_ _|  \\/  | ____| \\ | / ___|_ _|/_/ | \\ | |    | ____| \\ | |        \n" +Jugador.VERDE+
"| | | | || |\\/| |  _| |  \\| \\___ \\| |/ _ \\|  \\| |    |  _| |  \\| |        \n" +Jugador.VERDE+
"| |_| | || |  | | |___| |\\  |___) | | |_| | |\\  |    | |___| |\\  |        \n" +Jugador.VERDE+
"|____/___|_|  |_|_____|_| \\_|____/___\\___/|_| \\_|    |_____|_| \\_|        \n" +Jugador.VERDE+
"                    |  _ \\    / \\\\ \\ / // \\                               \n" +Jugador.VERDE+
"                    | |_) |  / _ \\\\ V // _ \\                              \n" +Jugador.VERDE+
"                    |  _ <  / ___ \\| |/ ___ \\                             \n" +Jugador.VERDE+
"                    |_|_\\_\\/_/   \\_\\_/_/__ \\_\\                            \n" +Jugador.VERDE+
"                      \\ \\/ /          / _ \\                               \n" +Jugador.VERDE+
"                       \\  /   _____  | | | |                              \n" +Jugador.VERDE+
"                       /  \\  |_____| | |_| |                              \n" +Jugador.VERDE+
"                      /_/\\_\\          \\___/                               "+Jugador.RESET);
    }
    
    public void limpiarPantalla(){
        System.out.println();System.out.println();System.out.println();System.out.println();System.out.println();System.out.println();System.out.println();
        System.out.println();System.out.println();
    }
}